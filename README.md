# Ansible Role: MySQL Setup for MailSteward Pro

Installs and configures MySQL for use with [MailSteward Pro](https://mailsteward.com).

## Requirements

- Tested on Ubuntu 20.04.

## Role Variables

Available variables are listed below:

```yaml
    mysql_password: root
```

## Usage

```yaml
- hosts: myserver
  become: true
  roles:
    - { role: mailsteward, tags: ["mailsteward"] }
```

## License

[Unlicense](https://unlicense.org)

## Author Information

This role was created in 2021 by [Pete Crocker](http://petecrocker.com/).
